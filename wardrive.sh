#!/bin/bash


#pkill gpsd
pkill kismet

ifconfig wlan1 down
ifconfig wlan2 down
ifconfig wlan3 down

gpsd -n 172.16.42.1:9999

gpspipe -w | grep -qm 1 '"mode":3'
UTCDATE=$(gpspipe -w | grep -m 1 "TPV" | sed -r 's/.*"time":"([^"]*)".*/\1/' | 
sed -e 's/^\(.\{10\}\)T\(.\{8\}\).*/\1\2/')

date -u -s "$UTCDATE"

iwconfig wlan1 mode Monitor 
iwconfig wlan2 mode Monitor
iwconfig wlan3 mode Monitor

kismet -p /root/kismetlogs -t wardrive --override wardrive -c wlan1 -c wlan2 -c wlan3
#/usr/bin/kismet_server
